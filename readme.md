# Test Opratel #

Documentación del Test Opratel para los web services 
API REST con Node/Express y SOAP/XML con PHP.

### API REST - NODE JS / EXPRESS ###

* Entrar en test-opratel/rest-node/ 
* Correr node service.js 
* La API REST se iniciará en [http://localhost:81/api](http://localhost:81/api)

### SOAP/XML - PHP / OOP ###

* Entrar en test-opratel/soap-php/ws
* Correr php -S localhost:82 
* Se iniciará el web service SOAP/XML en [http://localhost:82/?wsdl](http://localhost:82/?wsdl)
* El archivo service.php se encuentra en test-opratel/soap-php/ws/services/service.php 

### Datos de prueba y test ###

* En la carpeta test-opratel/data esta el archivo users.json de prueba que utilizan los dos web services.
* Se probo la api-rest con POSTMAN y el web service en soap con php con exito. 