<?php
require_once "helpers/nusoap/lib/nusoap.php";
require_once "services/service.php";

$namespace = "http://localhost:82/index.php";

// create a new soap server
$server = new \soap_server();

// configure our WSDL
$server->configureWSDL("testOpratel");

$server->wsdl->addComplexType(
    'UserObject',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' => array('name' => 'username', 'type' => 'xsd:string'),
        'email' => array('name' => 'email', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type'=>'xsd:string')
    )
);
// set our namespace
$server->wsdl->schemaTargetNamespace = $namespace;

// register the class method and the params of the method
$server->register("service.addUser"
    ,array('username'=>'xsd:string','password'=>'xsd:string' ,'email'=>'xsd:string')
    ,array('status_code'=>'xsd:string')
    ,$namespace,false
    ,'rpc'
    ,'encoded'
    ,'Metodo de agregar nuevo usuario o reemplazar datos de usuario existente'
);

// register the class method and the params of the method
$server->register("service.deactivateUser"
    ,array('username'=>'xsd:string')
    ,array('status_code'=>'xsd:string')
    ,$namespace,
    false
    ,'rpc'
    ,'encoded'
    ,'Metodo para desactivar un usuario.'
);

// register the class method and the params of the method
$server->register("service.activateUser"
    ,array('username'=>'xsd:string')
    ,array('status_code'=>'xsd:string')
    ,$namespace,
    false
    ,'rpc'
    ,'encoded'
    ,'Metodo para activar un usuario.'
);

// register the class method and the params of the method
$server->register("service.getUser"
    ,array('username'=>'xsd:string')
    ,array('user'=>'tns:UserObject')
    ,$namespace,
    false
    ,'rpc'
    ,'encoded'
    ,'Metodo para obtener los datos de un usuario por el username. Puede tanto activos como no activos. No se agrego logica adicional a este metodo.'
);


// pass our posted data (or nothing) to the soap service
$server->service(file_get_contents("php://input"));
exit();