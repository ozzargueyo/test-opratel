<?php

class Users{

    /**
     * FUNCIONES QUE OBTIENEN DATOS DE UN RESOURCE COMUN AL WS EN REST Y NODE
     */
    private $data = [];
    const DATA_RESOURCE = __DIR__."\..\..\..\data\users.json";


    public function setData($data = []){
        $this->data = $data;
    }

    private function loadData(){

        $data_array = [];
        try{
            $data_json = file_get_contents(self::DATA_RESOURCE);
            $data_json_array = json_decode($data_json);
            foreach ($data_json_array as $user){
                $data_array[$user->username] = (array) $user;
            }

        }catch(\Exception $e){
            $data_array = [];
        }

        $this->setData($data_array);
    }


    public function getData($id_usuario = NULL){
        $this->loadData();
        if($id_usuario){
            return (isset($this->data[$id_usuario])) ? (array)$this->data[$id_usuario] : 0;
        }
        return $this->data;
    }


    public function validateData($username = '' , $email = '' , $password = ''){

        $valid = true;
        if  ($username == '' || $email == '' || $password == '' ){
            $valid = false;
        }
        return $valid;

    }


    public function saveUser($username = '' , $data = []){

        $users = $this->getData();

        if ($username && isset($users[$username])){
            $users[$username]["username"] = $username;
            if (isset($data["email"])) $users[$username]["email"] = $data["email"];
            if (isset($data["password"])) $users[$username]["password"] = $data["password"];
            if (isset($data["isActive"])) $users[$username]["isActive"] = $data["isActive"];

        }else{
            $users[$username] = array(
                "username" => $username,
                "email" => $data["email"],
                "password" => $data["password"],
                "isActive" => true, // Los nuevos usuarios se agregan activos
            );
        }

        try{
            //Guardamos el archivo con los cambios
            return (file_put_contents(self::DATA_RESOURCE, json_encode(array_values($users)))) ? 0:1;
        }catch (\Exception $e){
            return 1;
        }

    }

}