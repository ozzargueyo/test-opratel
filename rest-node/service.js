//SERVER.JS
// endpoint de nuestra api rest

//SETUP

var express = require("express");
var bodyParser = require("body-parser");
var app = express();


var users = require("./resources/users");
//Config de app para usar body parser y obtener datos desde POST

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(bodyParser());

var port = process.env.PORT || 81; //Elijo 81 para la api rest con node, 80 fue para el soap

//Rutas

var router = express.Router(); //Instancia del ruteador de express

/**
 * Agregue este metodo para revisar si el archivo .json era actualizado correctamente
 */
router.get("/" , function (req, res) {
    res.json({data: users.getData()});
});

/**
 * Metodos del websevice requeridos en el test de acuerdo al test con sus respectivas acciones y metodos HTTP
 */

router.put("/addUser" , function(req , res){

    var username = req.body.username;
    //Seteamos la informacion del usuario
    var data = {
        'username': username,
        'email':req.body.email,
        'password':req.body.password,
        'isActive':true //Por defecto todos los usuarios estan activos
    };
    
    res.json(users.saveUser(username , data));
});

router.post("/activateUser" , function(req , res){
    var username = req.body.username;
    res.json(users.saveUser(username , {'isActive' : true}));
});

router.post("/deactivateUser" , function(req , res){
    var username = req.body.username;
    res.json(users.saveUser(username , {'isActive' : false}));
});

router.get("/getUser" , function(req , res){
    //Considero que el username viene en la url como query string y no en la uri
    var username = req.query.username;
    res.json(users.getUser(username));
});

//Registro de rutas las ponemos bajo un prefijo /api para ordenar
app.use("/api" , router);

//Comienzo de server
app.listen(port , function(){
    console.log("API REST en puerto "+port );
});


