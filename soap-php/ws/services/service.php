<?php
require_once __DIR__ . "\..\/resources\users.php";

class Service {

    public $source;

    function __construct()
    {
        $this->source = new Users();
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $email
     * @return int status_code 0 si es OK 1 si no pudo realizarse la accion
     */
    public function addUser($username = '', $password = '' , $email = '')
    {
        $status_code = 0;
        if (!$this->source->validateData($username , $password , $email )){
            //La info no es valida no hay nada mas que hacer, retornamos status_code = 1
            return 1;
        }

        /*
         * Al ser un metodo PUT se actualizaran todos los valores del usuario si es que existe, si no existe se creara uno nuevo con
         * esos valores.
         */
        $status_code = $this->source->saveUser($username , ["username" => $username , "password" => $password , "email" => $email]);
        return $status_code;

    }

    /**
     * @param string $username
     * @return int
     */
    public function activateUser($username = '')
    {
        $user = $this->getUser($username);

        if (count($user)> 0){
            //El usuario es valido y existe;

            return $this->source->saveUser($username , ["isActive" => true]);

        }

        return 1;
    }

    /**
     * @param string $username
     * @return int
     */
    public function deactivateUser($username = '')
    {
        $user = $this->getUser($username);

        if (count($user)> 0){
            //El usuario es valido y existe;

            return $this->source->saveUser($username , ["isActive" => false]);

        }

        return 1;
    }

    /**
     * @param string $username
     * @return array|int|mixed
     */
    public function getUser($username = ''){

        $user = $this->source->getData($username);
        unset($user["isActive"]); //En el requirimiento del webservice solo se pide que tenga username,password y email
        return ($user) ?  $user: [];
    }

}
