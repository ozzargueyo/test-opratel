var fs = require("fs");
var users_file = __dirname + '\\..\\..\\data\\users.json';

var self = {

    writeData: function(data){
        try {
            var keys = Object.keys(data);
            var content = JSON.stringify(keys.map(function(v) { return data[v]; }));
            fileContents = fs.writeFileSync(users_file, content, 'utf-8');
        } catch (err) {
            //Si capturo un error al momento de escribir el archivo no pudo salvar el usuario y retorna falso.
            return false
        }
        //Guardo el archivo correctamente y el usuario fue modificado o agregado con exito.
        return true;
    },

    getData : function (){
        var json = fs.readFileSync(users_file, 'utf8');
        var array = JSON.parse(json);
        var users = new Object();

        for(u in array) {
            users[array[u]["username"]] = array[u];
        }
        //Lo devolvemos como un diccionario clave valor para propositos del test, solo buscamos por clave y obtenemos el usuario.
        return users;

    },
    getUser : function (username ) {
        var users = self.getData();
        var user_selected = users[username];

        if(user_selected === undefined){
            return {};
        }

        return {"username": user_selected["username"], "password": user_selected["password"] , "email": user_selected["email"] };
    },

    saveUser : function(username , data){
        //Validamos si la informacion fue bien enviada, se agrego al probar en POSTMAN que los datos enviados por form-data
        // no pasaban y solo los application/x-www-form-urlencoded al usar el modulo de body-parser
        if ( typeof username === 'undefined' ) {
            return {status_code:1};
        }

        var rs = {
            status_code:0
        };

        var users = self.getData();

        if(!(username in users)){
            //Al tener un diccionario solo creo otro par clave-valor y lo guardo
            users[username] = data;
        }

        //Existe el usuario significa que solo se quiere cambiar alguno de sus atributos, compruebo cuales
        // y luego los seteo.

        if('email' in data) users[username]['email'] = data['email'];
        if('password' in data) users[username]['email'] = data['password'];
        if('isActive' in data) users[username]['isActive'] = data['isActive'];

        //Si no pudo escribir es que no pudo guardar los usuarios con los cambios, cambia el status_code a 1 y termina
        if (!self.writeData(users)){
            rs.status_code = 1;
        }

        //Guarda con exito o no y retorna el status_code correspondiente
        return rs;
    }
};

module.exports = self;